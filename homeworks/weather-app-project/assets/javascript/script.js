// Creating elements that contain our generated api key and the necessary url for the info we need
const apiKey = "53455db25f0e0807ddf7c5d5d76ae89a";
const apiURL = "https://api.openweathermap.org/data/2.5/weather?&units=metric&q=";


// Defining search input, button and icon elements
const searchInput = document.querySelector(".search-input");
const searchBtn = document.querySelector(".search-btn");
const weatherIcon = document.querySelector("#weather-icon");


// console.log(searchInput.value);
// console.log(searchBtn);


// Adding event listener to search button
searchBtn.addEventListener("click", function () {
    weatherCheck(searchInput.value);
});


// Creating a function to fetch all the necessary data from the api url we're using
async function weatherCheck(city) {
    const response = await fetch(apiURL + city + `&appid=${apiKey}`);

    // Data variable will contain all of the details we need for the project
    var data = await response.json();

    // Checking if the function is working
    console.log(data);

    // Changing html elements text accordingly
    document.querySelector(".cityName").innerHTML = data.name;
    document.querySelector(".temperature").innerHTML = data.main.temp + "°C";
    document.querySelector(".windSpeed").innerHTML = data.wind.speed + " km/h";
    document.querySelector(".humidity").innerHTML = data.main.humidity + "%";


    // Changing weather icon and background according to the weather
    if (data.weather[0].main == "Clouds") {
        weatherIcon.src = "./assets/img/cloud.png";
        document.body.style.backgroundImage = "url('./assets/img/cloudy-weather.jpg')";
    } else if (data.weather[0].main == "Rain") {
        weatherIcon.src = "./assets/img/rain.png";
        document.body.style.backgroundImage = "url('./assets/img/rainy-weather.jpg')";
    } else if (data.weather[0].main == "Mist") {
        weatherIcon.src = "./assets/img/fog.png";
        document.body.style.backgroundImage = "url('./assets/img/misty-weather.jpg')";
    } else if (data.weather[0].main == "Drizzle") {
        weatherIcon.src = "./assets/img/drizzle.png";
        document.body.style.backgroundImage = "url('./assets/img/drizzly-weather.jpg')";
    } else if (data.weather[0].main == "Clear") {
        weatherIcon.src = "./assets/img/sun.png";
        document.body.style.backgroundImage = "url('./assets/img/sunny-weather.jpg')";
    } else if (data.weather[0].main == "Snow") {
        weatherIcon.src = "./assets/img/snow.png";
        document.body.style.backgroundImage = "url('./assets/img/snowy-weather.jpg')";
    }
}

// weatherCheck();