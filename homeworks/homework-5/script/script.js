// ~~~~~~~~~~~ INSTRUCTIONS ~~~~~~~~~~~
// Create a dynamic table

// Write a Javascript function that will dynamiclly create a table
// User should input the number of Colums and Rows
// In every table cell print which row and column it is (ex. Row-3 Column-1)


const container = document.querySelector("div");
console.log(container);

// function rowsAndColumns(element) {
//     let rowNumber = parseInt(prompt("How many rows will your table have?"));
//     let columnNumber = parseInt(prompt("How many columns will your table have?"));

//     console.log(rowNumber);
//     console.log(columnNumber);

//     for(let i = 0; i < rowNumber; i++){
//         element.innerHTML += `<tr>`;
        
//         for(let x = 0; x < columnNumber; x++) {
//             element.innerHTML += `<td>Column</td>`;
//         }

//         element.innerHTML += `</tr>`;
//     }
// }

function createTable() {

    // rowsAndColumns(container);
    let rowNumber = parseInt(prompt("How many rows will your table have?"));
    let columnNumber = parseInt(prompt("How many columns will your table have?"));

    let htmlToAdd = ``;

    for(let i = 0; i < rowNumber; i++){
        htmlToAdd += `<tr>`;
        
        for(let x = 0; x < columnNumber; x++) {
            htmlToAdd += `<td>Column</td>`;
        }

        htmlToAdd += `</tr>`;
    }

    container.innerHTML += `<table border="1" cellpadding="50" cellspacing="10" style="text-align: center;">` + htmlToAdd + `</table>`;
}

createTable();