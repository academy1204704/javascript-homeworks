// For this exercise you need to create an Album list like the one in the image. You should write two constructor functions. The first one is MusicAlbum with the following properties (name, artist, artistImage, songs, yearReleased, albumCover, albumDuration) and two methods (addSong) to add the songs to your album and the second method (listAllSongs) to display all songs on the screen just like in the image.
// The second constructor function is Song with the following properties (title, artist, plays, duration).
// After that create the Album and create a couple of Songs. Add the Songs to the Album and list all songs on the screen.
// Use the image from the Spotify album as a reference for the structure, but keep in mind that you can use an album and an artist of your choice if you like.
// Bonus(but not required): Style the HTML just like in the image.


// Creating first constructor function for the entire playlist

// function MusicAlbum(name, user, userIcon, songs, yearReleased, playlistCover, playlistDuration)

function MusicAlbum(name, user, userIcon, songsNumber, yearReleased, playlistCover, playlistDuration) {
    // adding properties
    this.name = name;
    this.user = user;
    this.userIcon = userIcon;
    this.songsNumber = songsNumber;
    this.yearReleased = yearReleased;
    this.playlistCover = playlistCover;
    this.playlistDuration = playlistDuration;

    this.songs = [];

    this.printAlbum = function () {
        const playlistHeader = document.querySelector("#playlist-header");

        let htmlToAdd = `
        <div class="playlist-container">
            <img src="${this.playlistCover}" alt="playlist cover" id="playlist-cover" />

            <div class="playlist-text">
                <h5>Playlist</h5>

                <h1>${this.name}</h1>

                <div class="user-info">
                    <img src="${this.userIcon}" alt="user icon" id="user-icon" />
                    <h6>${this.user}</h6>
                    <h6>${this.yearReleased}</h6>
                    <h6>${this.songsNumber}</h6>
                    <h6>${this.playlistDuration}</h6>
                </div>
            </div>
        </div>
        `;

        playlistHeader.innerHTML = htmlToAdd;
    }

    // First method for adding a song to the songs array
    this.addSong = function (song) {
        this.songs.push(song);
    };

    // Second method to display all of the songs in the playlist
    this.listAllSongs = function () {
        const element = document.querySelector("#playlist-songs");

        let htmlToAdd = "";
        let index = 0;

        for (let song of this.songs) {
            htmlToAdd += `
                <li data-index="${index}">
                    <div id="item-container">
                        <div id="title-artist">
                            <h6>${song.title}</h6>
                            <h6>${song.artist}</h6>
                        </div>

                        <div id="song-plays">
                            <p>${song.plays}</p>
                        </div>

                        <div id="song-duration">
                            <p>${song.duration}</p>
                        </div>
                    </div>
                    
                </li>
            `;
            index++;
        }

        element.innerHTML = htmlToAdd;
    };
}

function Song(title, artist, plays, duration) {
    this.title = title;
    this.artist = artist;
    this.plays = plays;
    this.duration = duration;
}


// testing
// MusicAlbum(name, user, userIcon, songs, yearReleased, playlistCover, playlistDuration)
const newPlaylist = new MusicAlbum("Playlist Name", "sashka", "./assets/img/icon.jpg", "19 songs, ", "", "./assets/img/cover.jpg", "1 hr 25 min");
const song1 = new Song("(Don't Fear) The Reaper", "Blue Oyster Cult", "535,325,686", "5:08");
const song2 = new Song("Wheel in the Sky", "Journey", "126,198,507", "4:12");
const song3 = new Song("Carry on Wayward Son", "Kansas", "620,701,143", "5:23");
const song4 = new Song("Heat Of The Moment", "Asia", "205,088,715", "3:47");
const song5 = new Song("Message In A Bottle", "The Police", "462,547,273", "4:50");
newPlaylist.addSong(song1);
newPlaylist.addSong(song2);
newPlaylist.addSong(song3);
newPlaylist.addSong(song4);
newPlaylist.addSong(song5);
newPlaylist.listAllSongs();
newPlaylist.printAlbum();