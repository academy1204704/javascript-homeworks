// ~~~~~~~~~~~~~~~~  INSTRUCTIONS  ~~~~~~~~~~~~~~
// Improve Register User exercise

// Under the inputs in the HTML create a table element with 4 columns for First Name, Last Name, Password and E-mail
// Change the register user exercise code. Instead of printing the values in console, try to create a new table row and fill it with that values
// You should have a new table row for every new user you make 



// Finding and declaring all inputs
let firstName = document.querySelector("#firstName");
let lastName = document.querySelector("#lastName");
let email = document.querySelector("#email");
let password = document.querySelector("#password");
let button = document.querySelector("button");
let table = document.querySelector("table");
console.log(table);

// Creating function to handle new table columns
function inputData(name, surname, email, pass) {
    table.innerHTML += `<tr>
        <td>${name}</td>
        <td>${surname}</td>
        <td>${email}</td>
        <td>${pass}</td>
    </tr>`
}

// Adding event listener to button
button.addEventListener("click", function () {
    let firstNameValue = firstName.value;
    let lastNameValue = lastName.value;
    let emailValue = email.value;
    let passwordValue = password.value;

    inputData(firstNameValue, lastNameValue, emailValue, passwordValue);
}, false)