// List of elements' text to change: (first div) h1 & p, (second div) p & span, (third div): h1 & h3

let firstDiv = document.getElementById("first");
console.log(firstDiv);

let secondDiv = document.querySelector(".anotherDiv");
console.log(secondDiv);

let divs = document.getElementsByTagName("div");
let thirdDiv = divs[2];
console.log(thirdDiv);
// console.log(divs);



// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
let firstDivHeader = document.getElementById("myTitle");
// console.log(firstDivHeader);
firstDivHeader.innerText = "";
firstDivHeader.innerText += "Homework One";


let firstDivParagraph = document.querySelector(".paragraph");
// console.log(firstDivParagraph);
firstDivParagraph.innerText = "";
firstDivParagraph.innerText += "Welcome to my first javascript homework!";



let secondDivParagraph = document.querySelector(".second");
// console.log(secondDivParagraph);
secondDivParagraph.innerText = "";
secondDivParagraph.innerText += "It doesn't seem to be that difficult.";


let secondDivSpan = document.querySelector("span");
// console.log(secondDivSpan);
secondDivSpan.innerText += " changing their text."



let thirdDivH1 = thirdDiv.firstElementChild;
// console.log(thirdDivH1);
thirdDivH1.innerText = "";
thirdDivH1.innerText += "That's all.";


let thirdDivH3 = thirdDiv.lastElementChild;
// console.log(thirdDivH3);
thirdDivH3.innerText = "";
thirdDivH3.innerText += "Have a nice day!";