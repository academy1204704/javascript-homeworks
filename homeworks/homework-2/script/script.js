// Task: 
// Create an array with numbers
// Print all numbers from the array in a list element, in a new HTML page
// Print out the sum of all of the numbers below the list
// Bonus: Try printing the whole mathematical equasion as well ( 2 + 4 + 5 = 11)

let numbers = [
    12,
    56,
    89,
    564,
    238,
    2000,
    1
];
console.log(numbers);

let div = document.querySelector("div");
console.log(div);


function printNumbers(element, numbers) {
    element.innerHTML = "";
    element.innerHTML += "<ul>";
    for (let x = 0; x < numbers.length; x++) {
        element.innerHTML += `<li>${numbers[x]}</li>`;
    }
    element.innerHTML += "</ul>";
}

printNumbers(div, numbers);



// ~~~~~~~~~~~~~~~~~~~~~~~~~~
// function sumNumbers(element, numbers) {
//     element.innerHTML += "<br />";
//     for (let i = 0; i <= numbers.length; i++){
//         if(i < numbers[numbers.length - 1]){
//             element.innerHTML += `<span>${numbers[i]} + </span>`;
//         } else {
//             element.innerHTML += `<span>${numbers[i]}</span>`;
//         }

//     }
// }

// sumNumbers(div, numbers);
let sum = 0;
function printSum(element, numbers) {
    element.innerHTML += "<br />";
    for (let i = 0; i < numbers.length; i++) {
        sum += numbers[i];
    }
    element.innerHTML += `<p> The sum of all the numbers in the list is ${sum}.</p>`
}

printSum(div, numbers);