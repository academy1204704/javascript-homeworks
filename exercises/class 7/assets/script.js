function hide() {
    alert("The hide function is executed");
}

// let button = document.querySelector(".firstbtn");
// button.onclick = hide;



function countRabbits() {
    for (let i = 1; i <= 3; i++) {
        alert("Rabbit number " + i);
    }
}



let inputUser = document.querySelector(".inputUser");
let inputBtn = document.querySelector(".input-btn");

function checkUsername() {
    // code to check the length of a username
    if (inputUser.value == "admin") {
        alert("Welcome, admin!");
    } else {
        alert("Wrong user");
    }
}

// inputBtn.onclick = checkUsername;



const elem = document.getElementById("btnHello");

// elem.onclick = function () {
// alert("Hello, World!");
// }



// function sayHello(){
// alert("Hello, World!");
// }

// right
// elem.onclick = sayHello;

// wrong
// elen.onclick = sayHello();

function iWon() {
    alert("I'm the winner");
}

elem.onclick = iWon;



// ~~~~  EVENT LISTENER  ~~~~
inputBtn.addEventListener("click", checkUsername, false);

inputUser.addEventListener("keypress", function (event) {
    if (event.key === "Enter") {
        checkUsername();
    }
});



const inputUsername = document.querySelector("#username");

// inputUsername.addEventListener("blur", function () {
//     const greeting = `Hello ${inputUsername.value}`;
//     alert(greeting);
// });

function greetingFunc() {
    const greeting = `Hello ${inputUsername.value}`;
    alert(greeting);
}

// inputUsername.addEventListener("blur", greetingFunc);

// inputUsername.addEventListener("blur", function (event) {
//     const greeting = `Hello ${event.target.value}`;
//     // const greeting = `Hello ${event.inputUsername.value}`;
//     alert(greeting);
// });



const redElement = document.getElementById("redDiv");

function setColorToDiv(event) {
    event.target.style.backgroundColor = "red";
    // redElement.style.backgroundColor = "red";
}

redElement.addEventListener("mousemove", setColorToDiv);

redElement.removeEventListener("mousemove", setColorToDiv);



// ~~~~  USING PARAMETERS   ~~~~
const elUsername = document.getElementById("usernameInput");

const elMsg = document.getElementById("feedback");

function checkUsernameInput(minLength) {
    if (elUsername.value.length < minLength) {
        elMsg.textContent = `Username must be ${minLength} characters or more`;
    } else {
        elMsg.innerHTML + "";
    }
}

elUsername.addEventListener("blur", function () {
    checkUsernameInput(5);
}, false);



// Exercise #1 
let changeBtn = document.querySelector(".changeBtn");
let paragraph = document.querySelector(".text");

function changeStyle() {
    paragraph.style.fontSize = "24px";
    paragraph.style.color = "green";
    paragraph.style.fontStyle = "italic";
}

changeBtn.onclick = changeStyle;