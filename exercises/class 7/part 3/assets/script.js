// console.log(window.innerWidth);
// console.log(window.innerHeight);
// let width = window.innerWidth;
// let height = window.innerHeight;
// console.log(width);
// console.log(height);

let paragraph = document.querySelector("p");
console.log(paragraph);

// ~~~~~~~~~ MY SOLUTION ~~~~~~~~~~~~
// addEventListener("resize", function () {
//     this.document.write(`Width: ` + width + ` Height: ` + height);
// });

// My alternative solution ~~~~~~~~~~~
// addEventListener("resize", function (){
//     paragraph.innerHTML = `Width: ${width}  Height: ${height}`;
// })

// Alternative
window.addEventListener("resize", function (event) {
    // console.log(event);
    // console.log(event.target);
    paragraph.innerText = `Width: ${event.target.innerWidth} x Height: ${event.target.innerHeight}`;
    // event.target.x can also be window.x in this case (as long as the function has no parameters)
});