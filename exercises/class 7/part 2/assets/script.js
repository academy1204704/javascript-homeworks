let nameInput = document.querySelector("#firstName");
console.log(nameInput);

let surnameInput = document.querySelector("#lastName");
console.log(surnameInput);

let emailInput = document.querySelector("#email");
console.log(emailInput);

let passInput = document.querySelector("#password");
console.log(passInput);

let mainBtn = document.querySelector("#mainBtn");
console.log(mainBtn);

let paragraph = document.querySelector("p");


// function values(a, b, c, d) {
//     a = a.value;
//     b = b.value;
//     c = c.value;
//     d = d.value;
//     console.log(a + b + c + d);
// }

// values(nameInput, surnameInput, emailInput, passInput);

// function printValues() {
//     document.write = `${nameInput.Value}` + ` ~${surnameInput.value}` + ` ${emailInput.value}` + ` ${passInput}`;
// }



// ~~~~~~~~~~~~~~ MY SOLUTION ~~~~~~~~~~~~~~~~~
// mainBtn.addEventListener("click", function () {
//     paragraph.innerHTML += `<b>Full name:</b> ${nameInput.value} ${surnameInput.value}`;

//     paragraph.innerHTML += `<br />`;

//     paragraph.innerHTML += `<b>Email:</b> ${emailInput.value}`;

//     paragraph.innerHTML += `<br />`;

//     paragraph.innerHTML += `<b>Password:</b> ${passInput.value}`;
// }, false);
// console.log(emailInput.value);



// Alternative
function printInputs(firstName, lastName, email, password) {
    paragraph.innerText = `Full name: ${firstName} ${lastName}, Email: ${email}, Password: ${password}`;
};

mainBtn.addEventListener("click", function () {
    let firstValue = nameInput.value;
    let secondValue = surnameInput.value;
    let thirdValue = emailInput.value;
    let fourthValue = password.value;

    printInputs(firstValue, secondValue, thirdValue, fourthValue);
});