function makeRecipe() {
    let recipeName = prompt("Input the name of your recipe:");

    let numberOfIngredients = parseInt(prompt("How many ingredients will we need?"));

    let ingredients = [];

    for (let i = 0; i < numberOfIngredients; i++) {
        let singleIngredient = prompt("List the name of your ingredient.");
        ingredients.push(singleIngredient);
    }

    printRecipe(recipeName, numberOfIngredients, ingredients);
}



function printList(ingredients, hostElement) {
    let htmlAsString = "<ul>";
    for (let i = 0; i < ingredients.length; i++) {
        htmlAsString += `<li> ${ingredients[i]} </li>`;
    }

    htmlAsString += "</ul>";

    hostElement.innerHTML += htmlAsString;
}



function printRecipe(recipeName, numberOfIngredients, ingredients) {
    const hostRecipe = document.querySelector(".hostRecipe");

    let message = `
    <h2>${recipeName}</h2>
    
    <h3>For the preparation of ${recipeName}, we will need ${numberOfIngredients} ingredients.</h3>
    `;

    hostRecipe.innerHTML = message;
    printList(ingredients, hostRecipe);
}



makeRecipe();