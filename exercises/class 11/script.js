$(function () {
    console.log("ready");

    $('input[type="text"]').on('focus blur', function () {
        console.log("The user focused or blurred the input.");
    });

    $(window).on('resize scroll', function () {
        console.log("the window has been resized or scrolled.");
    });

    let handleClick = function () {
        console.log("Something was clicked.");
    }

    $("input").on("click", handleClick);
    $("#clickBtn").on("click", handleClick);

    $('a').on('click', function (event) {
        event.preventDefault;
        alert("The page didn't reload.");
    });
});