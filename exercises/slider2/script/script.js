$(document).ready(function () {
    console.log("Ready!");

    let slideNumber = 0;

    jquerySlider(slideNumber);

    function jquerySlider(index) {
        const slides = $('.slide');

        if (index >= slides.length) {
            slideNumber = 0;
        }

        if (index < 0) {
            slideNumber = slides.length - 1;
        }

        slides.hide();
        slides.eq(slideNumber).show();
    }

    $('.previous').on('click', function () {
        jquerySlider(slideNumber -= 1);
    });

    $('.next').on('click', function () {
        jquerySlider(slideNumber += 1);
    });

    // 'eq' means equals to
})