/* var day1;
let day2;
const day3 = "Monday"; */

// console.log(day3);

var day1;
let day2;
const day3 = "Monday";

console.log(day3);

// Defining a string variable
let text = "Task 01";
console.log(text);
console.log(typeof text);

// Defining a number variable
let num = 8;
console.log(num);
console.log(typeof num);

// Defining an undefined variable
let test;
console.log(test);
console.log(typeof test);

// Defining a boolean variable
let bool = true;
console.log(bool);
console.log(typeof bool);

// Defining a NULL variable
let number = null;
console.log(number);
console.log(typeof number);

// Undefined vs NULL example
/* let TestVar;
alert(TestVar); */

let TestVar = null;
// alert(TestVar);


let number1 = 2.2;
let number2 = 8.75;
let number3 = 234.756;

let name = "John";
let surname = "Doe";

// Operations
let a = 5;
let b = 10;

console.log(a + b);
console.log(a - b);
console.log(a * b);
console.log(a / b);
console.log(3 % 2);

let c = 3;
c++
console.log(c);

c--
console.log(c);

let x = 5;
let y = 5;
console.log(x != y);



// Exercise #1
let alias = "Lyra"
let year = "1995"
let sentence = alias + " " + year;
console.log(typeof sentence);

let JSmentor = false;
console.log(JSmentor);

let colors = ['blue', 'yellow', 'red'];
console.log(colors[1]);

// Declaring an object
let student = {
    ime: "Sashka",
    prezime: "Petrovska",
    godina: 2003,
};

console.log(student.ime);



// Ex. 1.1
let firstName = "Jane";
let lastName = "Smith";

document.write("<h1>" + firstName + " " + lastName + "</h2>");


// Exercise 2.1 (mine)
/* var i;
var i = +prompt("Please enter a value in feet:")
document.write(i * 0.3048 + " meters"); */


// Exercise 2.2
const feetLength = 0.3048;
let feet = 49;

let meters = feetLength * feet;
console.log(meters);


// Exercise 3 (mine)
let p = 5;
let k = 15;

let area = p * k;
console.log(area);


// Exercise 4.1 (mine)
/* const pi = 3.14;
let r = 6;
let r2 = r * r;

let circleArea = pi * r2;
console.log(circleArea); */

// Exercise 4.2 
const pi = 3.14;
let r = 15;
let circleArea = pi * (r * r);
console.log("Ploshtinata na krugot e: " + circleArea);