// Class #1 repetition exercises
let unknown = null;
let studentName = "Damjan";
let studentSurname = "Petrovski";
let birthDate = 2001;
let car;

console.log(unknown);
console.log(studentName + " " + studentSurname);
console.log(birthDate);


let value = true;
console.log(value);
console.log(typeof value);


let object = {
    name: "Jane",
    surname: "Doe",
    birthyear: 2000,
};
console.log(object);
console.log(typeof object);


let student = {
    studentName: "Damjan",
    studentSurname: "Petrovski",
    birthDate: 2001,
    email: "damjan.petrovski@gmail.com",
    phoneNumber: "+11111111",
    activeStudent: true
}
console.log(student.studentName + " " + student.studentSurname);


// Strings - concatenation
console.log("SEDC" + " " + "2019");

let result = "42" - 20;
console.log(result);
console.log(typeof result);

let result1 = "Hello" - 42;
console.log(result1);

const sedc = "SEDC";
console.log(sedc + " " + 2018);


// Template literals & expressions
let result2 = `Hello ${sedc} Academy 2018`;
console.log(result2);

let year = 2024;
let result3 = `Copyright ${year} SEDC Academy`;
console.log(result3);


// Quotes in strings
let text = 'It\'s really nice to be a programmer';
console.log(text);


// Comparison operations
let a = 5;
let b = 10;

console.log(a > b);
console.log(a >= b);
console.log(a < b);
console.log(a <= b);


// Equality operations
let c = "5";
let d = "Hello";
let e = 5;
let f = "Hello";

console.log(a == b);
console.log(a == c);
console.log(a == d);

console.log(a != b);
console.log(a === b);
console.log(a === c);
console.log(c === d);
console.log(a === e);
console.log(c !== d);
console.log(d !== f);
console.log(c == d);


// Logical operations
let expr1 = true;
let expr2 = false;
let expr3 = true;
let expr4 = false;

console.log(expr1 && expr2);
console.log(expr1 && expr3);
console.log(expr1 || expr2);
console.log(expr2 || expr4);
console.log(!expr1);
console.log(!expr2);


// Structure example
let pass = 50;
let score = 90;
let hasPassed = score >= pass;
console.log(hasPassed);


// ~~~~~~~~~   Logical && (and) example   ~~~~~~~~~

let a1 = true && true;
console.log(a1);
// t && t returns true

let a2 = true && false;
console.log(a2);
// t && f returns false

let a3 = false && true;
console.log(a3);
// f && t returns false

let a4 = false && false;
console.log(a4);
// f && f returns false

let a8 = "" && false;
console.log(a8);
// f && f returns false
// "" && f returns ""

let a9 = false && "";
console.log(a9);
// f && "" returns false


// ~~~~~~~   Logical || (or) example   ~~~~~~~
let o1 = true || true;
console.log(o1);
// t || t returns true

let o2 = false || true;
console.log(o2);
// f || t returns true

let o3 = true || false;
console.log(o3);
// t || f returns true;

let o4 = false || false;
console.log(o4);
// f || f returns false

let o8 = '' || false;
console.log(o8);
// "" || f returns false

let o9 = false || "";
console.log(o9);
// f || "" returns ""


// ~~~~~~ Logical ! (not) operations ~~~~~~~
let n1 = !true;
console.log(n1);
// !t returns false

let n2 = !false;
console.log(n2);
// !f returns true

let n3 = !"Cat";
console.log(n3);
// !"" returns false


// == and === dilemma example
let dilemma = 0 == false;
console.log(dilemma);

let dilemma1 = 0 === false;
console.log(dilemma1);


// Inequality example
let q = 41;
let k = "42";
let p = "43";

console.log(q < k);
console.log(k < p);


let y = 42;
let v = "foo";

console.log(y < v);
// 42 < NaN returns false

console.log(y > v);
// 42 > NaN returns false

console.log(y == v);
// 42 == NaN returns false