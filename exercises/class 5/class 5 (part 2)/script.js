let myApp = document.getElementById("app");
let titleDiv = myApp.firstChild;
let contentDiv = myApp.children[1];
let contentDivStudents = myApp.children[2];

let students = ["Bob Bosky",
    "Jill Cool",
    "John Doe",
    "Jane Sky"
];

let subjects = ["Math",
    "English",
    "Science",
    "Sport"
];

let grades = ["A",
    "B",
    "A",
    "C"
];

function printGrades(subjects, grades, element) {
    // element.innerHTML = "";
    let innerTags = `<ul>`;
    element.innerHTML += "<ul>";
    for (let i = 0; i < subjects.length; i++) {
        innerTags += `<li>${subjects[i]}: ${grades[i]}</li>`;
    }
    innerTags += `</ul>`;
    element.innerHTML += innerTags;
}

function printStudents(students, element) {
    // element.innerHTML = "";
    let innerTags = "";
    innerTags += "<ol>";
    for (let student of students) {
        innerTags += `<li>${student}</li>`;
    }
    innerTags += "</ol>";
    element.innerHTML += innerTags;
}

// printGrades(subjects, grades, contentDiv);
// printStudents(students, contentDivStudents);



function academyPanel(person, name) {
    let text = "";
    if (person === "student" && name.length >= 2) {
        text = "<h1> <b> Hello There " + name + "</b> </h1>";

        text += "<p> Welcome to your student page </p>";

        contentDiv.innerHTML += "<h3> Your grades: </h3>";

        printGrades(subjects, grades, contentDiv);

        titleDiv.innerHTML = text;
    } else if (person === "teacher" && name.length >= 2) {
        text = `<h1> <b> Hello ${name} </b> </h1>
        <p> Welcome to your teachers page </p>`;

        contentDiv.innerHTML += "<h3>Your students: </h3>";

        printStudents(students, contentDiv);

        titleDiv.innerHTML = text;
    } else {
        text = "<h1> <b> Your login was unsucessfull </b> </h1>";
        
        text += "<p> Please try again! </p>";
    }
}

let input = prompt("Are you a student or a teacher?");
let nameOfPerson = prompt("What is your name?");

academyPanel(input, nameOfPerson);