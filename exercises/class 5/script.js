// Getting elements by their ID
let myHeader = document.getElementById("myTitle");
console.log(myHeader);
console.log(myHeader.innerText);



// Getting elements by their class
let paragraphs = document.getElementsByClassName("myParagraph");
let secondParagraph = document.getElementsByClassName("second");

console.log(paragraphs);
console.log(paragraphs[0]);
console.log(secondParagraph);
console.log(secondParagraph[0]);



// Getting elements by their tag name
let paragraphsByTagName = document.getElementsByTagName("p");
console.log(paragraphsByTagName);
console.log(paragraphsByTagName[1]);

let texts = document.getElementsByTagName("text");
console.log(texts);
console.log(texts[0]);



// Query selectors
let paragraphsWithQuerySelectors = document.querySelectorAll("p");
console.log(paragraphsWithQuerySelectors);
console.log(paragraphsWithQuerySelectors[0]);

let firstP = document.querySelector(".myParagraph");
console.log(firstP);

let header = document.querySelector("#myTitle");
console.log(header);



// Finding sibling elements
let para = document.getElementsByClassName("myParagraph")[0];
console.log(para);

let sibling = para.nextElementSibling;
console.log(sibling);



// Finding parent elements
let parentdiv = para.parentElement;
console.log(parentdiv);



// Finding child elements
let myDiv = document.getElementById("main");
let divChildren = myDiv.children;
console.log(divChildren);

let divFirstChild = myDiv.firstElementChild;
console.log(divFirstChild);

let divLastChild = myDiv.lastElementChild;
console.log(divLastChild);



// Getting an element's text
let textSpaces = myDiv.textContent;
console.log(textSpaces);

let textNoSpaces = myDiv.innerText;
console.log(textNoSpaces);



// Changing an element's text
console.log(header.innerText);

header.innerText = "";
console.log(header.innerText);

header.innerText = "NEW TEXT!";
console.log(header.innerText);

header.innerText += "Ultra new text";
console.log(header.innerText);



// Changing and adding code
console.log(myDiv);

myDiv.innerHTML += `<p class="new">
Paragraph generated from Javascript
</p>`;
console.log(myDiv);

// removing all code   ->   myDiv.innerHTML = "";
// console.log(myDiv);