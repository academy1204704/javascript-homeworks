let first = document.getElementById("first");
console.log(first);

let allParagraphs = document.querySelectorAll("p");
console.log(allParagraphs);

// let divs = document.getElementsByTagName("div");
// console.log(divs);

let lastDiv = document.getElementsByTagName("div")[2];
// let lastDiv = document.querySelectorAll("div")[2]; - alternative
console.log(lastDiv);

let lastDivHeadingThree = lastDiv.children[1];
console.log(lastDivHeadingThree);

let lastDivHeadingOne = lastDiv.firstElementChild;
console.log(lastDivHeadingOne);

let secondDivPara = document.getElementsByClassName("second");
console.log(secondDivPara.innerText);

let text = document.querySelector("span").innerText;
text.innerText += " text";
console.log(text);

lastDivHeadingOne.innerText = "This is changed";
console.log(lastDivHeadingOne);

lastDivHeadingThree.innerText = "This is also changed!";
console.log(lastDivHeadingThree);