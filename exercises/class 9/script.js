const selectContainer = document.querySelector(".selectCountriesContainer");


fetch('./countries.json')
    .then(response => response.json())
    .then(data => populateSelect(data))
    .catch(error => console.error('Error fetching JSON:', error));

function populateSelect(data) {

    data.forEach(element => {
        const option = document.createElement("option");

        option.value = element.code;
        option.textContent = element.name;

        selectContainer.appendChild(option);
    });

}

// DOG API
function getRandomDog() {
    fetch('https://dog.ceo/api/breeds/image/random')
        .then(response => response.json())
        .then(data => printDogImage(data.message))
        .catch(error => console.log("Error fetching API: ", error));

    function printDogImage(imageUrl) {
        let dogResultContainer = document.querySelector('.dog-result');

        dogResultContainer.innerHTML = `<img src="${imageUrl}" alt="dog" />`;
    }
}

let dogBtn = document.querySelector('.getDogBtn');
dogBtn.addEventListener("click", getRandomDog, false);