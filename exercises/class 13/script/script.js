// MY ATTEMPT
// // Main constructor function
// function CarLot(name) {
//     this.name = name;
//     this.cars = [];

//     // Method for adding to the car list
//     this.addCar = function (car) {
//         this.cars.push(car);
//     };

//     // Method for listing the cars
//     this.listCars = function () {
//         const carsList = document.querySelector('#car-list');

//         let htmlToAdd = ``;

//         for (let car of this.cars) {
//             htmlToAdd += `<li>
//             ${car.model}, ${car.year}, ${car.color}, ${car.fuel}, ${car.fuelConsumption}, ${car.distance}
//             </li>`;
//         }

//         carsList.innerHTML = htmlToAdd;

//         // const addBtn = document.querySelector('#add-btn');
//     };
// }

// // Add car constructor function
// function Car(model, year, color, fuel, fuelConsumption, distance) {
//     this.model = model;
//     this.year = year;
//     this.color = color;
//     this.fuel = fuel;
//     this.fuelConsumption = fuelConsumption;
//     this.distance = distance;
// }

// // Testing
// const myCarLot = new CarLot("Placeholder");
// const addBtn = document.querySelector('#add-btn');
// addBtn.addEventListener("click", function(e) {
//     e.preventDefault();
//     const model = document.querySelector("#model").value;
//     const year = document.querySelector("")
// })








// Class version - alternative
function CarLot(name) {
    this.name = name;
    this.cars = [];

    this.addCar = function (car) {
        this.cars.push(car);
    };

    this.listCars = function () {
        const element = document.querySelector('#car-list');

        let htmlToAdd = ``;

        const actionBtns = `<button class="delete-btn">Delete</button>
        <button class="calc-btn">Calculate</button>`;

        for (let car of this.cars) {
            htmlToAdd += `<li id="${car.id}">
            ${car.color}, ${car.model}, ${car.year}, ${car.fuelConsumption}, ${car.fuel} ${actionBtns}
            </li>`;
            // htmlToAdd += `<div style="color: ${car.color};">${car.model}, ${car.year}, ${car.fuelConsumption}, ${car.fuel} ${actionBtns}</div>`;
        }

        element.innerHTML = htmlToAdd;
    };

    this.calculateFuelConsumption = function () {
        const car = this.cars.find(element => element.id === id);

        if (car) {
            return (car.fuelConsumption * car.distance) / 100;
        }
    }
}

function Car(model, year, color, fuel, fuelConsumption, distance) {
    this.id = Date.now();
    this.model = model;
    this.year = year;
    this.color = color;
    this.fuel = fuel;
    this.fuelConsumption = fuelConsumption;
    this.distance = distance;
}


const myCarLot = new CarLot("SEDC Rent a Car");
const addBtn = document.querySelector('#add-btn');

addBtn.addEventListener('click', function (e) {
    e.preventDefault();
    const model = document.querySelector('#model').value;
    const year = document.querySelector('#year').value;
    const color = document.querySelector('#color').value;
    const fuel = document.querySelector('#fuel').value;
    const fuelConsumption = document.querySelector('#fuel-consumption').value;
    const distance = document.querySelector('#distance').value;

    myCarLot.addCar(new Car(model, year, color, fuel, fuelConsumption, distance));
    myCarLot.listCars();
})

document.addEventListener('click', function () {
    if (e.target.classList.contains('delete-btn')) {
        // delete
    } else if (e.target.classList.contains('calc-btn')) {
        const carId = parseInt(e.target.parentElement.id);
        console.log(myCarLot.calculateFuelConsumption(carId));
    }
});