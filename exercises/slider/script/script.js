let slideNumber = 0;

const prevBtn = document.querySelector('.prev');
const nextBtn = document.querySelector('.next');

function slideFunction(index) {
    const slides = document.querySelectorAll('.slide');

    if (index >= slides.length) {
        slideNumber = 0;
    }

    if (index < 0) {
        slideNumber = slides.length - 1;
    }

    for (let i = 0; i < slides.length; i++) {
        slides[i].style.display = "none";
    }

    slides[slideNumber].style.display = "block";
}

function previousSlide() {
    slideFunction(slideNumber -= 1);
}

prevBtn.addEventListener('click', previousSlide);
nextBtn.addEventListener('click', nextSlide)