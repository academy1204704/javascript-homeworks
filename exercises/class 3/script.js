function myfunction() {
    // loops, ifs, statements, anything....
    console.log("Hello World");
}

myfunction();
myfunction();
myfunction();

// Functions with parameters (values inside the first brackets)
function myFunction1(x, y) {
    let myVar = x * y;
    console.log(myVar);
}

myFunction1(1, 3);
myFunction1(2, 5);
myFunction1(20, 55);
myFunction1(100, -2400);


// Returning values
function myfunction2(x, y) {
    let myVar = x * y;

    return myVar;
}

let result = myfunction2(5, 10);
console.log(result);


function fullName(firstName, lastName) {
    return firstName + " " + lastName;
}

let firstName = "Petar";
let lastName = "Pavlovski";

let fullNameResult = fullName(firstName, lastName);
let fullNameResult1 = fullName(lastName, firstName);
console.log(fullNameResult);
console.log(fullNameResult1);


// Exercise #1 
function sumNumbers(x, y) {
    let value = x + y;
    return "The result is: " + value;
}

console.log(sumNumbers(2, 5));
console.log(sumNumbers(10, 15));


// Exercise #2 - temperature converter 
function celsiusToFahrenheit(temp) {
    let fahrenheit = (temp * 9) / 5 + 32;
    return `${temp}C is ${fahrenheit}F`;
}

console.log(celsiusToFahrenheit(16));


function fahrenheitToCelsius(temp) {
    let celsius = ((temp - 32) * 5) / 9;
    return `${temp}F is ${celsius}C`;
}

console.log(fahrenheitToCelsius(58));


// Exercise #3 - the fortune teller
function tellFortune(x, y, z, n) {
    return `You will be a ${x} in ${y}, and married to ${z} with ${n} kids.`
}

console.log(tellFortune("baker", "New Zealand", "John Smith", 3));
console.log(tellFortune("florist", "Japan", "Jane Doe", 2));
console.log(tellFortune("librarian", "Sweden", "Bob Brown", 5));


// Control structures
let number = 14;

if (typeof (number) == "number") {
    console.log("The variable is a number");
}



let score = 100;

if (score > 100) {
    // alert("You won!");
    console.log("You won!");
} else if (score < 100) {
    // alert("You lose...");
    console.log("You lose...");
} else {
    // alert("It's a tie.");
    console.log("It's a tie.");
}


// Exercise #4 
// let allowance = +prompt("How large is your budget for this weekend?");
let allowance = 50;

if (allowance > 50) {
    console.log("You could go to the cinema and have a nice dinner.")
} else if (allowance < 50) {
    console.log("Maybe you could watch a movie at home instead..");
} else {
    console.log("You could watch a movie at the cinema.");
}

// Ex. #4 alternative
// let budget = prompt("What is your allowance?")

// if (budget >= 50) {
console.log("You can travel to another city!");
// } else if (budget >= 30) {
// console.log("You can visit an art gallery~");
// } else if (budget >= 20) {
// console.log("You can go to the park.");
// } else {
// console.log("You can have a movie night at home..")
// }


// Exercise #5
function greaterNum(x, y) {
    if (x > y) {
        return `The number with a higher value is ${x}.`;
    } else if (x < y) {
        return `The number with a higher value is ${y}.`;
    } else {
        return `Both numbers have an equal value.`;
    }
}

console.log(greaterNum(2, 5));
console.log(greaterNum(10, 5));
console.log(greaterNum(50, 50));

// Ex. #5 alternative
function greaterNum2(num1, num2) {
    let max;
    if (num1 > num2) {
        max = num1;
    } else {
        max = num2;
    }
    return `The number with the highest value among ${num1} and ${num2} is ${max}.`;
}

console.log(greaterNum2(20, 60));


// Exercise #6 
function assignGrade(result) {
    if (result >= 100) {
        return `Your grade is: A`;
    } else if (result >= 75) {
        return `Your grade is: B`;
    } else if (result >= 65) {
        return `Your grade is: C`;
    } else if (result >= 45) {
        return `Your grade is: D`;
    } else {
        return `You have failed the exam.`
    }
}

console.log(assignGrade(100));
console.log(assignGrade(80));
console.log(assignGrade(70));
console.log(assignGrade(50));
console.log(assignGrade(20));


// Ex. #6 alternative
function assignGrade2(score) {
    if (score >= 90 && score <= score) {
        return "A";
    } else if (score >= 80 && score <= 89) {
        return "B";
    } else if (score >= 70 && score <= 79) {
        return "C";
    } else if (score >= 60 && score <= 69) {
        return "D";
    } else if (score >= 0 && score <= 59) {
        return "F";
    } else {
        return "Unsupported input";
    }
}

let grade = assignGrade2(73);
console.log(grade);


// Exercise #7
function helloWorld(language) {
    if (language == "mk") {
        console.log("Zdravo, Svetu!");
    } else if (language == "es") {
        return `Hola, Mundo`;
    } else if (language == "de") {
        return `Hallo, Welt!`;
    } else {
        console.log("Hello, World!")
    }
}

let lang = "DE";
let convertedText = lang.toLocaleLowerCase(lang);
helloWorld(convertedText);

helloWorld("mk");
helloWorld("fr");

// Exercise #8 
function chineseZodiac(year) {
    let result = (year - 4) % 12;

    switch (result) {
        case 0:
            console.log("Rat");
            break;
        case 1:
            console.log("Ox");
            break;
        case 2:
            console.log("Tiger");
            break;
        case 3:
            console.log("Rabbit");
            break;
        case 4:
            console.log("Dragon");
            break;
        case 5:
            console.log("Snake");
            break;
        case 6:
            console.log("Horse");
            break;
        case 7:
            console.log("Goat");
            break;
        case 8:
            console.log("Monkey");
            break;
        case 9:
            console.log("Rooster");
            break;
        case 10:
            console.log("Dog");
            break;
        case 11:
            console.log("Pig");
            break;
        default:
            console.log("Invalid input");
            break;
    }
}

chineseZodiac(2003);