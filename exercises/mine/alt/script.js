const firstName = document.querySelector("#firstName");
const lastName = document.querySelector("#lastName");
const email = document.querySelector("#email");
const password = document.querySelector("#password");
const button = document.querySelector("button");

console.log(firstName);

// function getValues (one, two, three, four){
//     let firstNameValue = one.value;
//     let lastNameValue = two.value;
//     let emailValue = three.value;
//     let passwordValue = four.value;

//     document.write(``);
// }

function writeString (one, two , three, four){
    let message = `Full name: ${one} ${two}, User email: ${three}, User password: ${four}`;
    document.write(`<p>` + message + `</p>`);
}

button.addEventListener("click", function(){
    let firstNameValue = firstName.value;
    let lastNameValue = lastName.value;
    let emailValue = email.value;
    let passwordValue = password.value;

    writeString(firstNameValue, lastNameValue, emailValue, passwordValue);
}, false);