// let p = document.getElementsByClassName("examplePara");
// let section = document.getElementsByClassName("exampleSection");
// let article = document.getElementsByClassName("exampleArticle");

// console.log(p);
// console.log(section);
// console.log(article);



// ~~~~~~~~~~~~~~~~~~~~~~
// let text = document.getElementsByTagName("p");
// let container = document.getElementsByTagName("div");

// console.log(text);
// console.log(container);



// ~~~~~~~~~~~~~~~~~~~~~~
// let paras = document.querySelectorAll("p");
// let firstPara = document.querySelector("p");
// let article = document.querySelector("#arti");
// let section = document.querySelector(".exampleSection");

// console.log(paras);
// console.log(firstPara);
// console.log(article);
// console.log(section);



// ~~~~~~~~~~~~~~~~~~~~~~
// let p = document.querySelector("p");
// let h1 = p.previousElementSibling;
// let span = p.nextElementSibling;
// let article = p.parentElement;
// let section = article.parentElement;

// console.log(p);
// console.log(h1);
// console.log(span);
// console.log(article);
// console.log(section);



// ~~~~~~~~~~~~~~~~~~~~~~
// let article = document.getElementById("arti");
// let artChildren = article.children;
// let firstChild = article.firstElementChild;
// let lastChild = article.lastElementChild;

// console.log(article);
// console.log(artChildren);
// console.log(firstChild);
// console.log(lastChild);



// ~~~~~~~~~~~~~~~~~~~~~~
// let p = document.querySelector("p");
// let paras = document.querySelector("section");

// console.log(p.innerText);
// console.log(p.textContent);
// console.log(p.innerHTML);

// console.log(paras.innerText);
// console.log(paras.textContent);
// console.log(paras.innerHTML);
// let header = document.querySelector("h1");
// console.log(header);
// console.log(header.innerText);

// header.innerText = "";
// console.log(header.innerText);

// header.innerText = "New Exercise";
// console.log(header.innerText);

// header.innerText += " Title";
// console.log(header.innerText);


// let cont = document.querySelector("#exampleDiv");
// console.log(cont);

// cont.innerHTML += "<p>Hello, World!</p>";
// console.log(cont.innerText);



// ~~~~~~~~~~~~~~~~~~~~~~
let students = ["Bob Bobsky", "Jill Cool", "John Doe", "Jane Sky"];
let subjects = ["Math", "English", "Science", "Sport"];
let grades = ["A", "B", "A", "C"];

function printGrades(subjects, grades, element) {
    console.log(element);
    element.innerHTML = "";
    element.innerHTML += "<ul>"
    for (let i = 0; i < subjects.length; i++) {
        element.innerHTML += `<li>${subjects[i]}: ${grades[i]}</li>`;
    }
    element.innerHTML += "</ul>"
}

function printStudents(students, element) {
    element.innerHTML = "";
    element.innerHTML += "<ol>"
    for (let x = 0; x < students.length; x++) {
        element.innerHTML += `<li>${students[x]}</li>`
    }
    element.innerHTML += "</ol>"
}

let div = document.querySelector("#exampleDiv");
// printGrades(subjects, grades, div);
printStudents(students, div);