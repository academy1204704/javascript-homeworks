let colors = ["red", "orange", "yellow", "pink"];
// console.log(colors[0]);
// console.log(colors[1]);
// console.log(colors[2]);
// console.log(colors[3]);
colors[colors.length] = "purple";
colors.push("blue", "teal");
colors.push("green");
colors.unshift("brown");
colors.unshift("black", "magenta");

colors.splice(1, 3, "white",
    "grey");
console.log(colors);


let months = [
    "January",
    "February",
    "March",
    "April",
    "May",
    "June",
    "July",
    "August",
    "September",
    "October",
    "November",
    "December"];


console.log(months);
console.log(months[1]);
// months[1] = "Feb";
// console.log(months[1]);
console.log(months.length);
console.log("length of the year: " + months.length + " months");
console.log(months[months.length - 1]);


let emptyList = [];
console.log(emptyList);
console.log(emptyList.length);


let zodiac = ["aries",
    "taurus",
    "gemini",
    "cancer",
    "leo",
    "virgo"];
console.log(zodiac);
console.log(zodiac[1]);
console.log(zodiac[zodiac.length - 1]);
zodiac.push("libra");
// console.log(zodiac.length);
zodiac.splice(2, 0, "aquarius");

let removedZod = zodiac[1];
zodiac.splice(1, 1, "capricorn");
zodiac.push(removedZod);