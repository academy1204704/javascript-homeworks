// Creating objects with constructor notation
let example = new Object();
example.firstPropety = "hello";
example.secondProperty = 25;
example.thirdProperty = false;

console.log(example);
// console.log(example.firstPropety);
// console.log(example.secondProperty);


// Creating objects with curly brackets (literal notation)
let hdm = {
    name: "His Dark Materials",
    author: "Philip Pullman",
    books: 3,
    booksNames: ["The Golden Compass", "The Subtle Knife", "The Amber Spyglass"]
}

console.log(hdm);
// console.log(hdm.books);
// console.log(hdm.booksNames);


// Object methods
let message = {
    number: 1,
    generator: function () {
        return `Hello, World!`;
    }
}

console.log(message);
// console.log(message["number"]);
// console.log(message.generator);


// Constructor function/method/notation for creating object templates
function Student(name, surname, gender, age) {
    this.name = name;
    this.surname = surname;
    this.gender = gender;
    this.age = age;
}

let firstStudent = new Student("Jane", "Doe", "female", 24);
let secondStudent = new Student("John", "Smith", "male", 19);
let thirdStudent = new Student("Ash", "Rosewood", null, 26);
// console.log(firstStudent);
// console.log(secondStudent);
// console.log(thirdStudent);


// Using "this" to check window size
function windowSize() {
    const width = this.innerWidth

    return [height, width];
}



//  Using a method to return an object's properties
const userInfo = {
    name: "Sophie Hatter",
    age: 20,
    occupation: "Hat store owner",
    details: function () {
        console.log(`${this.name}, ${this.age}, ${this.occupation}`);
    }
}
userInfo.details();



// Deleting a property from an object
const userInfo2 = {
    name: "Howl Pendragon",
    age: 24,
    occupation: "Wizard",
    profile: function () {
        console.log(`${this.name}, ${this.age}, ${this.occupation}`);
    }
}
userInfo2.profile();
delete userInfo2["age"];
userInfo2.profile();


// Creating a function to check if an object property exists
const storage = {
    handCreams: 4,
    moisturizer: 2,
    cleanser: 3
}

// function checkProperty(object, property) {
//     if (object.property !== undefined) {
//         return true;
//     } else return false;
// }
// checkProperty(storage, "cleanser");

function checkProperty(object, property) {
    if (object.hasOwnProperty(property)) {
        console.log("The object has the given property!");
    } else {
        console.log("The object does not have the given property...");
    }
}

checkProperty(storage, "cleanser");


// // Creating a car object with a method
// function Car(model, color, year, fuel, fuelConsumption) {
//     this.model = model;
//     this.color = color;
//     this.year = year;
//     this.fuel = fuel;
//     this.fuelConsumption = function(distance, ) {
//         let fuelUsed = distance
//     }
// }