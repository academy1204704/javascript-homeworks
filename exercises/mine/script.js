// define my store

function Store(name) {
    this.name = name; //name of the store
    this.products = []; //array of products
    this.shoppingCartProducts = []; // array of products that are in the shopping cart

    //methods

    //adding a product in the Store
    this.addProduct = function (product) {
        this.products.push(product);
    };

    // list the available products(in stock)
    this.listProducts = function () {
        const element = document.querySelector("#products");

        let htmlToAdd = "";
        let index = 0;

        for (let product of this.products) {
            htmlToAdd += `
                  <li data-index="${index}"> 
                      <h4>Name: ${product.name}</h4>

                      <div>
                        <img src="${product.imgUrl}" alt="${product.name}" class="product-img" />
                      </div>

                      <div id="quantity">Quantity: ${product.quantity}</div>

                      <div>Description: ${product.description}</div>

                      <div>Compare: 
                          <input type="checkbox" class="compare-check">
                      </div>

                      <div>
                        <button class="add-to-cart">Add to cart</button>
                      </div>
                  </li>
              `;
            index++;
        }

        element.innerHTML = htmlToAdd;
    };

    // Add product to Cart
    this.addToCart = function (product) {
        this.shoppingCartProducts.push(product);
        this.listProductsInShoppingCart();
    };

    // list the products in the shopping cart
    this.listProductsInShoppingCart = function () {
        const element = document.querySelector("#shopping-cart"); // <ol>
        let htmlToAdd = "";
        for (let item of this.shoppingCartProducts) {
            htmlToAdd += `<li>${item.name} - ${item.price} mkd</li>`;
        }
        element.innerHTML = htmlToAdd;
    };

    //compare the products
    this.compareProducts = function (product1, product2) {
        const element = document.querySelector("#compare");
        element.innerHTML = `
          <table border="2">
              <tr>
                  <th>${product1.name}</th>
                  <th>${product2.name}</th>
              </tr>
  
              <tr>
                  <td>${product1.description}</td>
                  <td>${product2.description}</td>
              </tr>
  
              <tr>
                  <td class="${product1.price < product2.price ? "cheaper-product" : ""
            }">${product1.price}</td>
                  <td class="${product2.price < product1.price ? "cheaper-product" : ""
            }">${product2.price}</td>
              </tr>
          </table>
          `;
    };

    //get product by provided index number
    this.getProductByIndex = function (index) {
        let product = this.products[index];
        if (product) {
            return product;
        } else {
            return false;
        }
    };
}

//define my products
function Product(name, price, imgUrl, description, quantity) {
    this.name = name;
    this.price = price;
    this.imgUrl = imgUrl;
    this.description = description;
    this.quantity = quantity;
}

//creating the product objects
const moonLamp = new Product("VGAzer Levitating Moon Lamp", 4500, "https://th.bing.com/th/id/OIP.-_3bZNMHME6KOp-ZdKj3JQHaHa?rs=1&pid=ImgDetMain", "Use Magnetic levitation technique,Suspended and Spinning automatically in mid-air Freely without any support or contact,able to attract people's eyes.", 2);
const moonNecklace = new Product("Moon Pendant Necklace", 1200, "https://th.bing.com/th/id/OIP.Nw2NoHe4m2Nf71WurlOz8AHaJ4?rs=1&pid=ImgDetMain", "Pre-owned condition with no notable flaws. All pre-owned items may show signs of light wear.", 5);
const moonPillow = new Product("Rectangular Moon Themed Pillow", 500, "https://ctl.s6img.com/society6/img/qTEVSypDtC3FfIcMgTbZQlhBnGE/w_700/pillows/~artwork,fw_3500,fh_3500,iw_3500,ih_3500/s6-0025/a/11166428_1472257/~~/moon-gs0-pillows.jpg", "Last in stock!", 1);

const myStore = new Store("SEDC Store");
myStore.addProduct(moonLamp);
myStore.addProduct(moonNecklace);
myStore.addProduct(moonPillow);

myStore.listProducts();

// Add to cart
document.addEventListener("click", function (e) {
    //   console.log(e.target);
    if (e.target.classList.contains("add-to-cart")) {
        // console.log("ima klasa add-to-cart");
        const productIndex = parseInt(
            e.target.closest("li").getAttribute("data-index")
        );

        const product = myStore.getProductByIndex(productIndex);
        if (product) {
            myStore.addToCart(product);
        }
    }
});

//lets compare some products
let productsToCompare = [];

document.addEventListener("click", function (e) {
    if (e.target.classList.contains("compare-check")) {
        e.target.disabled = true;
        const productIndex = parseInt(
            e.target.closest("li").getAttribute("data-index")
        );
        const product = myStore.getProductByIndex(productIndex);

        productsToCompare.push(product);
        if (productsToCompare.length === 2) {
            myStore.compareProducts(productsToCompare[0], productsToCompare[1]);
            productsToCompare = [];
            uncheckComapreInputs();
        }
    }
});

// uncheck the input buttons
function uncheckComapreInputs() {
    const inputs = document.querySelectorAll(".compare-check");
    for (let input of inputs) {
        input.checked = false;
        input.disabled = false;
    }
}
