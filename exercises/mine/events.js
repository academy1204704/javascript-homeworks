const button = document.querySelector("#btn");
// console.log(button);

// button.onclick = function(){
//     alert("Hello, World!");
// };

// function sayHello() {
//     alert("Hello, World!");
// }

// // button.onclick = sayHello;
// button.addEventListener("click", sayHello, false);



// ~~~~~~~~~~~~ Changing text w/ button
const text = document.querySelector(".bold");
// console.log(text);
function changeStyle(element) {
    element.style.color = "blue";
    element.style.fontSize = "30px";
    element.style.backgroundColor = "pink";
}

button.addEventListener("click", function () {
    changeStyle(text);
}, false);