// Constructor function - defining our store
function Store(name) {
    // store properties
    this.name = name;
    this.products = [];
    this.shoppingCartProducts = [];


    // First method to add a product to our available store products
    this.addProduct = function (product) {
        this.products.push(product);
    };


    // Second method for listing the actual available products on display
    this.listProducts = function () {
        const element = document.querySelector("#products");

        let htmlToAdd = "";
        let index = 0;

        for (let product of this.products) {
            htmlToAdd += `
            <li data-index="${index}">
                <h4>Product Name: ${product.name}</h4>

                <div>
                    <img src="${product.imgUrl} alt="${product.name}" />
                </div>

                <div id="quantity">
                    <h4>Quantity:</h4>
                    <p>${product.quantity}</p>
                </div>

                <div>
                    <h4>Description:</h4>
                    <p>${product.description}</p>
                </div>

                <div>
                    <h4>Compare:</h4>
                    
                    <input class="compare-checkbox" type="checkbox" />
                </div>

                <div>
                    <button class="add-to-cart-btn">Add to cart</button>
                </div>
            </li>
            `;

            index++;
        }

        element.innerHTML += htmlToAdd;
    };

    // Fourth method to add a certain displayed product to the shopping cart
    this.addToCart = function (product) {
        this.shoppingCartProducts.push(product);
        this.listProductsInShoppingCart();
    };

    // Third method to display all the newly added products in the shopping cart
    this.listProductsInShoppingCart = function () {
        const element = document.querySelector("#shopping-cart");
        let htmlToAdd = "";
        for (let item of this.shoppingCartProducts) {
            htmlToAdd += `<li>${item.name} - ${item.price} MKD</li>`;
        }

        element.innerHTML += htmlToAdd;
    };



    // Fifth method to compare two products
    this.compareProducts = function (product1, product2) {
        const element = document.querySelector("#compare");
        element.innerHTML = `
        <table border="2">
        
            <tr>
                <th>${product1.name}</th>
                <th>${product2.name}</th>
            </tr>
            
            <tr>
                <td>${product1.description}</td>
                <td>${product2.description}</td>
            </tr>
            
            <tr>
                <td class="${product1.price < product2.price ? "cheaper-product" : ""}">
                    ${product1.price}
                </td>

                <td class="${product2.price < product1.price ? "cheaper-product" : ""}">
                    ${product2.price}
                </td>
            </tr>
        
        </table>
        `;
    };


    // Sixth method to get a certain product by its index
    this.getProductByIndex = function (index) {
        let product = this.products[index];

        if (product) {
            return product;
        } else {
            return false;
        }
    };
}

function Product(name, price, imgUrl, description, quantity) {
    this.name = name;
    this.price = price;
    this.imgUrl = imgUrl;
    this.description = description;
    this.quantity = quantity;
}


// Using our constructor functions and building our store
const moonLamp = new Product("VGAzer Levitating Moon Lamp", 4500, "https://th.bing.com/th/id/OIP.-_3bZNMHME6KOp-ZdKj3JQHaHa?rs=1&pid=ImgDetMain", "Use Magnetic levitation technique,Suspended and Spinning automatically in mid-air Freely without any support or contact,able to attract people's eyes.", 2);
const moonNecklace = new Product("Moon Pendant Necklace", 1200, "https://th.bing.com/th/id/OIP.Nw2NoHe4m2Nf71WurlOz8AHaJ4?rs=1&pid=ImgDetMain", "Pre-owned condition with no notable flaws. All pre-owned items may show signs of light wear.", 5);
const moonPillow = new Product("Rectangular Moon Themed Pillow", 500, "https://ctl.s6img.com/society6/img/qTEVSypDtC3FfIcMgTbZQlhBnGE/w_700/pillows/~artwork,fw_3500,fh_3500,iw_3500,ih_3500/s6-0025/a/11166428_1472257/~~/moon-gs0-pillows.jpg", "Last in stock!", 1);

const myStore = new Store("Moonie Store");
myStore.addProduct(moonLamp);
myStore.addProduct(moonNecklace);
myStore.addProduct(moonPillow);
myStore.listProducts();


// Function to add the product to our shopping cart
document.addEventListener("click", function (e) {
    if (e.target.classList.contains("add-to-cart")) {
        const productIndex = parseInt(
            e.target.closest("li").getAttribute("data-index")
        );

        const product = myStore.getProductByIndex(productIndex);
        if (product) {
            myStore.addToCart(product);
        }
    }
});