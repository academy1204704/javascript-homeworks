// DO/WHILE LOOPS
let condition = 1;

do {
    console.log(condition);
    condition++;
} while (condition < 10);


// FOR LOOPS
let num = [
    12,
    654,
    2957,
    98,
];

for (let i = 0; i < num.length; i++) {
    console.log(num[i]);
}

// Example
let num1 = 1;

while (num1 <= 100) {
    console.log("Number " + num1 + "<br>");
    num1 += 1;
}

for (let num2 = 1; num2 <= 100; num2++) {
    document.write("Number " + num2 + "<br>");
}


// Example - for loops with arrays
let days = [
    "Mon",
    "Tue",
    "Wed",
    "Thu",
    "Fri",
    "Sat",
    "Sun"
];

for (let i = 0; i < days.length; i++) {
    console.log(days[i] + ", ");
}

let example = [
    "First",
    "Second",
    "Third",
    "Last"
];

for (let j = example.length; j > 0; j--) {
    console.log(example[j - 1]);
}

// Exercise
// let score = [45, 78, 94, 23];
// for (i = 0; i <score.length; i++){
    // if(score.length[i]>=60 && 
// }