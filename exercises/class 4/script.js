// ~~~   ARRAYS   ~~~
let colors = ["red", "blue", "green", "purple", "yellow"];
console.log(colors[0]);
console.log(colors[3]);


// Array example #1
let days = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"];
console.log(days[6]);


// Array example #2 - empty array
let iAmEmpty = [];
console.log(iAmEmpty[0]);


// Array example #3 - mixed array
let mixed = [1, 2, "I am a string", false];
console.log(mixed[0]);


// Array example #4 - whitespace and new lines
let authors = [
    "Ernest Hemingway",
    "Charlotte Bronte",
    "Dante Alighieri",
    "Emily Dickinson"
];
console.log(authors);


// Array example #5 - accesing items in an array
// let arrayName = [itemIndex];
console.log(days[1]);


// Example #6 - changing items' values
days[1] = "Monday";
console.log(days[1]);

console.log("length of days: " + days.length);

days[days.length - 1] = "Sun";
console.log(days[days.length - 1]);


// Example #7 - adding items
days[days.length] = "No more days in the week";
console.log(days[7]);

days.push("I said, no more days!");
console.log(days[days.length - 1]);

console.log(days);

days.push("Please stop adding days", "No more.....");
console.log(days[days.length - 1]);
console.log(days.length);


// Example #8 - adding items at the beginning of an array
let years = [
    2000,
    2001,
    2002,
    2003];
years.unshift(1999);
console.log(years[0]);

years.unshift(1995, 1996, 1997, 1998);
console.log(years);


// Example #9 - deleting items
let p = [0, 1, 2, 3];
let removedItem = p.pop();
console.log(removedItem);

let removedItemFromBeginning = p.shift();
console.log(removedItemFromBeginning);


// Example #10 - adding items at a specific position/index (splice)
let fruits = [
    "Banana",
    "Orange",
    "Apple",
    "Mango"
];
fruits.splice(2, 0, "Lemon", "Kiwi");
console.log(fruits);
console.log(fruits[2]);


// Exercise #1
let flowers = [
    "Rose",
    "Carnation",
    "Tulip",
    "Lily"
];
console.log(flowers[1]);
console.log(flowers[flowers.length - 1]);

flowers.push("Bluebell");
console.log(flowers.length);

flowers.splice(2, 0, "Sunflower");
flowers.splice(1, 0, "Marigold");

console.log(flowers);

// Exercise #1 - alternative
let fruits2 = [
    "Apple",
    "Banana",
    "Lemon",
    "Mango"
];
console.log(fruits2);
console.log(fruits2[1]);

console.log(fruits2[fruits2.length - 1]);

fruits2.push("Pear");
console.log(fruits2);

console.log(fruits.length);

fruits2.splice(2, 0, "Pineapple");
console.log(fruits2);

let reservedFruit = fruits2[1];
console.log(reservedFruit);

fruits2.splice(1, 1, "Kiwi");
console.log(fruits2);

fruits2.push(reservedFruit);
console.log(fruits2);


// ~~~   LOOPS   ~~~
let condition = 10;

while (condition <= 20) {
    console.log(condition);

    condition++;
}


// Loops example #1
let x = 1;

while (x < 10) {
    x = x + 1;

    console.log(x);
}


// Loops example #2 - mized with arrays
let daysOfTheWeek = [
    "Mon",
    "Tue",
    "Wed",
    "Thu",
    "Fri",
    "Sat",
    "Sun"
];

let counter = 0;

while (counter < daysOfTheWeek.length) {
    if (counter == daysOfTheWeek.length - 1) {
        document.write(daysOfTheWeek[counter] + ".");
    } else {
        document.write(daysOfTheWeek[counter] + ", ");
    }

    counter++;
}


// Exercise #2 (reading the element with the highest value)
let numbers = [
    12,
    654,
    2957,
    98,
    18,
    567,
    900,
    -35,
    4,
    23
];

let i = 0;
let max = numbers[0];

while (i < numbers.length) {
    if (max < numbers[i]) {
        max = numbers[i];
        console.log(max);
    }

    i++;
}

console.log("The maximum number of this array is: " + max);