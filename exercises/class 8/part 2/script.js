// Defining my store
function Store(name) {
    this.name = name;
    this.products = [];
    this.shoppingCartProducts = [];

    // method (for adding a product in the store)
    this.addProduct = function (product) {
        this.products.push(product);
    }

    // method (for listing the available products in stock)
    this.listProducts = function () {
        const element = document.querySelector("#products");

        let htmlToAdd = "";
        let index = 0;

        // for(let product of this. products) - alternative (for arrays)
        // let i = 0; i < this.products.length; i++
        // if using original method like above, "product" will be replaced with "this.products[i].PROPERTY"
        for (let product of this.products) {
            htmlToAdd += `
            <li data-index="${index}">
                <h4>Name: ${product.name}</h4>


                <div>

                    <img src="${product.imgUrl}" alt="${product.name}" />

                </div>


                <div id="quantity">

                    <h4>Quantity: ${product.quantity}</h4>

                </div>

                
                <div>
                
                    <h4>Description: ${product.description}</h4>

                </div>


                <div>
                
                    <h4>Compare:</h4>

                    <input type="checkbox" class="compare-check" />

                </div>


                <div>
                
                    <button class="add-to-cart-btn">
                        Add to cart
                    </button>

                </div>
            </li>
            `;
            index++;
        }

        element.innerHTML = htmlToAdd;
    }

    this.addToCart = function (product) {
        this.shoppingCartProducts.push(product);
        this.listProductsInShoppingCart();
    }

    // Method to list the products in the shopping cart
    this.listProductsInShoppingCart = function () {
        const element = document.querySelector("#shopping-cart");
        let htmlToAdd = "";
        for (let item of this.shoppingCartProducts) {
            htmlToAdd += `<li>${item.name} - ${item.price} MKD</li>`;
        }

        element.innerHTML = htmlToAdd;
    }

    // Comparing the products
    this.compareProducts = function (product1, product2) {
        const element = document.querySelector("#compare");
        element.innerHTML = `
        <table border="2">

            <tr>

                <th>${product1.name}</th>
                <th>${product2.name}</th>
            
            </tr>

            
            <tr>
            
                <td>${product1.description}</td>
                <td>${product2.description}</td>

            </tr>
            
            
            <tr>
            
                <td class="${product1.price < product2.price ? 'cheaper-product' : ''}">${product1.price}</td>

                <td class="${product2.price < product1.price ? 'cheaper-product' : ''}">${product2.price}</td>

            </tr>

        </table>
        `;
    }

    // Method for getting a product by its provided index number

    this.getProductByIndex = function (index) {
        let product = this.products[index];

        if (product) {
            return product;
        } else {
            return false;
        }
    }
}


// Method for defining my products
function Product(name, price, imgUrl, description, quantity) {
    this.name = name;
    this.price = price;
    this.imgUrl = imgUrl;
    this.description = description;
    this.quantity = quantity;
}


const phoneSamsung = new Product("Samsung S24", 60000, "https://jasystore.com/wp-content/uploads/2021/05/samsung_galaxy_s24_5g-1.jpg", "Released on January 22nd 2024", 10);
const phoneIphone = new Product("iPhone 15", 60000, "https://mobilepriceall.com/wp-content/uploads/2022/09/Apple-iPhone-14-1024x1024.jpg", "Released on October 13th 2023", 45);
const phoneMotorola = new Product("Motorola A15", 40000, "https://phonesdata.com/files/models/Oppo-A15-867.jpg", "Released on May 17th 2022", 22);

const myStore = new Store("SEDC Store");
myStore.addProduct(phoneSamsung);
myStore.addProduct(phoneIphone);
myStore.addProduct(phoneMotorola);

myStore.listProducts();

// contains is to see if there is a class
// parseInt is used to transform the value into a number
// Adding to cart
document.addEventListener("click", function (e) {
    // console.log(e.target);
    if (e.target.classList.contains("compare-check")) {
        const productIndex = parseInt(e.target.closest("li").getAttribute("data-index"));

        const product = myStore.getProductByIndex(productIndex);

        if (product) {
            myStore.addToCart(product);
        }
    }
});


// Comparing products
let productsToCompare = [];

document.addEventListener("click", function (e) {
    if (e.target.classList.contains("compare-check")) {
        e.target.disabled = true;
        const productIndex = parseInt(e.target.closest("li").getAttribute("data-index"))

        const product = myStore.getProductByIndex(productIndex);

        productsToCompare.push(product);

        if (productsToCompare.length === 2) {
            myStore.compareProducts(productsToCompare[0], productsToCompare[1]);

            productsToCompare = [];

            uncheckCompareInputs();
        }
    }
});

function uncheckCompareInputs() {
    const inputs = document.querySelector(".compare-check");

    for (let input of inputs) {
        input.checked = false;

        input.disabled = false;
    }
}