// Example #1
let playerName = "Fred";
let playerScore = 10000;
let playerRank = 1;

console.log(playerName);



// Example #2
let player = new Object();

player.name = "Fred";
player.Score = 10000;
player.Rank = 1;

console.log(player.name);



// Example #3
let hotel = {
    name: "Palace",
    rooms: 40,
    booked: 25,
    gym: true,
    roomTypes: ["twin", "double", "suite"],
    checkAvailability: function () {
        return this.rooms - this.booked;
    }
};

console.log(hotel.checkAvailability());
console.log(hotel.roomTypes[1]);
console.log(hotel.roomTypes[0]);
console.log(hotel.gym);
console.log(hotel);

// Defining object properties as variables
let hotelName = hotel.name;
console.log(hotelName);

let roomsFree = hotel.checkAvailability();
console.log(roomsFree);

// Acessing object properties with brackets (instead of the dot notation method)
let hotelNameAlternative = hotel["name"];
console.log(hotelNameAlternative);



// Example #4
let student = {
    name: "Marko",
    age: 19,
    class: "4-B",
    grades: ["A", "C", "B", "F", "B"],
    index: "125165",
    povedenie: false,
    zodiac: "Gemini",
    message: function () {
        console.log(`The name of the student is ${this.name} and he is ${this.age} years old, has an index number of ${this.index} and is a ${this.zodiac} sun.`);
    }
};

student.message();
// console.log(`The name of the student is ${student.name} and he is ${student.age} years old, has an index number of ${student.index} and is a ${student.zodiac} sun.`);



// Example #5
let programmer = new Object();

// console.log(programmer);

programmer.experience = "3 years";
programmer.languages = ["Javascript", "PHP", "Java", "Python"];
programmer.name = "Filip";
programmer.company = "Company name";

console.log(programmer);

// Deleting an object property
delete programmer.company;
console.log(programmer);



// Example #6 (square brackets)
const shape = {
    width: 600
};

shape["height"] = 400;

console.log(shape);



// Example #7 (empty object)
let emptyObject = {};
console.log(emptyObject);



// Example #8 (function constructor)
function Hotel(name, rooms, booked, gym, roomType, lakeView, spa) {
    this.name = name;
    this.rooms = rooms;
    this.booked = booked;
    this.gym = gym;
    this.roomType = roomType;
    this.lakeView = lakeView;
    this.spa = spa;

    this.checkAvailability = function () {
        return this.rooms - this.booked;
    }
}

// Object instances
const hotelGranit = new Hotel("Granit", 130, 74, true, ["single", "double"], true, false);

const hotelStruga = new Hotel("Struga", 56, 13, false, ["single", "double", "suite"], false, true);

console.log(hotelGranit);
console.log(hotelStruga);


function HotelNew(name) {
    this.nameOfHotel = name;
}

const newHotel = new HotelNew("Nov Hotel");
console.log(newHotel);



// Example #9
function windowSize() {
    const width = this.innerWidth;
    const height = this.innerHeight;

    return [height, width];
}

console.log(windowSize());

// A function in global scope
const width = 600;

function showWidth() {
    console.log(this.width);
}

showWidth();


// Example #10 (a method of an object)
const shapeAlt = {
    width: 600,
    height: 400,
    getArea: function () {
        return this.width * this.height;
    }
}

// console.log(shapeAlt.getArea());

const shapeAltTwo = {
    width: 600,
    height: 400,
    getArea: function () {
        function multiply() {
            console.log(this);
            return this.width * this.height;
        }

        console.log(this);
        return multiply();
    }
};

shapeAltTwo.getArea();



// Exercise #1
// const studentExample = {
//     name: "David Ray",
//     class: "VI",
//     rollno: 12,
//     output: function () {
//         // console.log(`${studentExample.name}, ${studentExample.class}, ${studentExample.rollno}`);
//         console.log(`${this.name}, ${this.class}, ${this.rollno}`);
//     }
// }

// studentExample.output();



// Exercise #2
const studentExample = {
    name: "David Ray",
    class: "VI",
    rollno: 12,
    // deleteRollno: function () {
    //     console.log(this.rollno);
    //     delete studentExample.rollno;
    //     console.log(this.rollno);
    // }
}

console.log(studentExample.rollno);
delete studentExample.rollno;
console.log(studentExample.rollno);
// studentExample.deleteRollno();



// Exercise #3 (mine)
const example = {
    element: 0
};

function checkProperty(object, property) {
    if (object.hasOwnProperty(property)) {
        console.log("The object has the given property!");
    } else {
        console.log("The object does not have the given property...");
    }
}

// checkProperty(example, element);
checkProperty(example, "element");



// Exercise #3 (alternative)
const exercise3 = {
    result: "x"
};

function objectPropertyCheck(object, property) {
    // Getting the property here below will not be possible with dot notation, since the program will directly take the property of the object with that same name (won't understand the parameter and will take it literally)
    console.log(object);

    // if(object[property] !== undefined) {
    //     return `It exists`;
    // } else {
    //     return `It doesn't exist`;
    // }

    
    // If/else statement alternative (see original above)
    return object[property] !== undefined ? "It exists" : "It doesn't exist";
}

console.log(objectPropertyCheck(exercise3, "result"));