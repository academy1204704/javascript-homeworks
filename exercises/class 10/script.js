$(document).ready(function () {

    console.log("document ready function");

    // $("div").addClass("colorized").hide();

    // jQuery("section").addClass("colorized");

    // $("div").addClass("new-color");

    $(".colorized").show();

    function showSendBtn() {
        const numberOfInputs = $("#contact-form input").length;
        let numberOfFilledInputs = 0;
        $("#contact-form input").each(function (index, element) {
            if (element.value != "") {
                numberOfFilledInputs++;

                if (numberOfFilledInputs == numberOfFilledInputs) {
                    $("#send-btn").slideDown();
                }
            }
        });
    }

    $("#contact-btn").on("click", function () {
        $("#contact-form").fadeIn(800);
    });

    $("#contact-form input").on("blur", function () {
        showSendBtn();
    });


    function clearContactForm() {
        $("#contact-form input").val("");
    }


    $("#send-btn").on("click", function (e) {
        e.preventDefault();

        $("#loading-img").fadeIn(800);
        $(this).fadeOut();

        $("#loading-img").fadeOut(3000);

        $(this).fadeIn(3100, function () {
            clearContactForm();
        });
    });

    // $("#start-btn").on("click", function() {
    //     $(".box:eq(0)").fadeIn(2000, function() {
    //         $(".box:eq(1)").fadeIn(2000, function() {
    //             $(".box:eq(2)").fadeIn(2000, function())
    //         } )
    //     })
    // })


    // $("#search-btn").on("click", function() {
    //     $("#search-input").fadeIn(500);
    // });

    // $("#search-btn").on("click", function() {
    //     // $("#search-input").fadeIn(500);
    //     $("#search-input").toggleClass("active-input");
    // });

    // $("div.child").toggleClass(function(){
    //     if($(this).parent(.is(".parent")))
    // });

});


// Alternative-shorter version of upper code
// $(function () {

// })