// console.log("ready");

// Adding main constructor function
function BookList(name) {
    // this.title = title;
    // this.author = author;
    // this.thumbnail = thumbnail;
    // this.price = price;
    // this.quantity = quantity;
    // this.description = description;
    this.books = [];

    this.bookAdd = function (book) {
        this.books.push(book);
    }

    // Adding to result area method
    this.add = function (book) {
        const element = document.querySelector("#books-list");

        let htmlToAdd = ``;

        htmlToAdd += `<li class="card">
            <div class="card-title">${book.title}</div>

            <div class="card-img">
                <img src="${book.thumbnail}">
            </div>

            <div class="card-description">${book.description}</div>

            <div class="card-price">${book.price}</div>

            <div class="card-quantity">${book.quantity}</div>

            <div class="card-actions">
                <button class="buy-btn btn buy" id="buy-btn"> buy </button>
                <button class="wishlist-btn btn"> add to wishlist </button>
            </div>
        </li>`;

        element.innerHTML = htmlToAdd;
    };

    // this.buyAction = function() {
    //     const buyBtn = document.querySelector("#buy-btn");
    //     const element = document.querySelector("#shopping-cart");

    //     // buyBtn.addEventListener("click", function(e) {
    //     //     e.preventDefault();

    //     //     const element = document.querySelector("#shopping-cart");
        
    //     //     let htmlToAdd = ``;
        
    //     //     htmlToAdd += `<li>${this.book.title}
    //     //         <a href="#" class="remove-book-cart">Remove</a>
    //     //     </li>`;
        
    //     //     element.innerHTML = htmlToAdd;
    //     // } );
    // }

};

// Html code
// <!-- <li id="1"> True nature. Part 2 John Doe 1500  <a href="#" class="remove-book-cart" id="1"> remove </a> </li> -->

// <!-- <li id="1" class="card">
//                         <div class="card-title"> True nature. Part 2 </div>
//                          <div class="card-img"> <img src="https://bit.ly/2PxhFm8"> </div>
//                          <div class="card-description"> Realy nice book and easy to read and cant res... </div>
//                          <div class="card-price"> Price: 1500 den.</div>
//                          <div class="card-quantity"> Quantity: 17 </div>
//                          <div class="card-actions">
//                             <button class="buy-btn btn buy"> buy </button>
//                             <button class="wishlist-btn btn"> add to wishlist </button>
//                          </div>
//                     </li> -->




// Second BOOK constructor function
function Book(title, author, thumbnail, price, quantity, description) {
    this.title = title;
    this.author = author;
    this.thumbnail = thumbnail;
    this.price = price;
    this.quantity = quantity;
    this.description = description;
};

const myBookList = new BookList("My Book List");


// Add button event listener
const addBtn = document.querySelector("#add-btn");
// console.log(addBtn);

addBtn.addEventListener("click", function (e) {
    e.preventDefault();
    const titleInput = document.querySelector("#title").value;
    const authorName = document.querySelector("#author").value;
    const thumbnailUrl = document.querySelector("#thumbnail").value;
    const priceTag = document.querySelector("#price").value;
    const quantityInput = document.querySelector("#quantity").value;
    const descriptionText = document.querySelector("#description").value;


    // Testing
    const myBook = new Book(titleInput, authorName, thumbnailUrl, priceTag, quantityInput, descriptionText);

    myBookList.bookAdd(myBook);
    myBookList.add(myBook);

    // console.log(myBook);
    // myBook.add();
    // myBook.buy();
});


// function addToShoppingCart(book) {
//     const element = document.querySelector("#shopping-cart");

//     let htmlToAdd = `<li>${book.title}, ${book.price} 
//         <a href="#" class="remove-book-cart" id="1"> remove </a>
//     </li>`;

//     element.innerHTML += htmlToAdd;
// };


// function buyButton() {
//     const book = myBookList.book;

//     addToShoppingCart(book);
// };

// document.addEventListener("click", function (e) {
//     if (e.target.classList.contains("buy-btn")) {
//         buyButton();
//     } else return `blank`; }
// );



// htmlToAdd += `<li>${this.book.title}
//         //         <a href="#" class="remove-book-cart">Remove</a>
//         //     </li>`;
        
        //     element.innerHTML = htmlToAdd;


// document.addEventListener("click", function (e) {
//     if (e.target.classList.contains("buy-btn")) {
//         e.preventDefault();
//         const element = document.querySelector("#shopping-cart");
    
//         let htmlToAdd = ``;
    
//         htmlToAdd += `<li>${myBookList.title}
//             <a href="#" class="remove-book-cart">Remove</a>
//         </li>`;
    
//         element.innerHTML = htmlToAdd;
//     } else if (e.target.classList.contains("wishlist-btn")) {
//         e.preventDefault();
//         const element = document.querySelector("#wishlist");
    
//         let htmlToAdd = ``;
    
//         htmlToAdd += `<li>${this.title}
//             <a href="#" class="remove-book-cart">Remove</a>
//         </li>`;
    
//         element.innerHTML = htmlToAdd;
//     }
//   });