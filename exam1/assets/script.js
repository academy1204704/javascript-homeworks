// Arrays
let users = ["user_one",
    "user_two",
    "user_three",
    "user_four"
];

let passwords = ["password1",
    "password2",
    "password3",
    "password4"
];

console.log(users);
console.log(passwords);

// Defining all elements 
const usernameInput = document.querySelector("#username");
const passwordInput = document.querySelector("#password");
const container = document.querySelector(".container");
const button = document.querySelector(".loginBtn");
const message = document.querySelector("p");

// console.log(usernameInput);
// console.log(passwordInput);


// Creating log in function

let success = `Successfully logged in!`;
let failure = `Username does not exist or wrong password.`;

function checkInputs() {
    if (usernameInput.value === users[0] && passwordInput.value === passwords[0]) {
        message.innerText = success + ` Welcome, ${users[0]}`;
    } else if (usernameInput.value === users[1] && passwordInput.value === passwords[1]) {
        message.innerText = success + ` Welcome, ${users[1]}`;
    } else if (usernameInput.value === users[2] && passwordInput.value === passwords[2]) {
        message.innerText = success + ` Welcome, ${users[2]}`;
    } else if (usernameInput.value === users[3] && passwordInput.value === passwords[3]) {
        message.innerText = success + ` Welcome, ${users[3]}`;
    } else {
        message.innerText = failure;
    }
}

// Adding event listener
button.addEventListener("click", checkInputs, false);